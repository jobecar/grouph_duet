<?php 
    include "../models/dbconnect.php";
    $password_check = null;
    $success_check = null;

    if(isset($_POST['submit'])){
        $name = mysqli_real_escape_string($conn, $_POST['name']);
        $age = mysqli_real_escape_string($conn, $_POST['age']);
        $gender = mysqli_real_escape_string($conn, $_POST['gender']);
        $email = mysqli_real_escape_string($conn, $_POST['email']);
        $mobile = mysqli_real_escape_string($conn, $_POST['mobile']);
        $username = mysqli_real_escape_string($conn, $_POST['username']);
        $account_type = "admin";
        $password = mysqli_real_escape_string($conn, $_POST['password']);
        $confirm_password = mysqli_real_escape_string($conn, $_POST['confirm']); 

        if($password==$confirm_password){
            $password_check = true;
            $query = "INSERT INTO user (name,age,gender,email,mobile_number,username,user_type,password)
                    VALUES ('$name','$age','$gender','$email','$mobile','$username','$account_type','$password');";

            $result = $conn->query($query);

            if($result){
                $success_check = true;
            } else {
                $success_check = false;
            }
        } else {
            $password_check = false;
        }
    }

    if(isset($_POST['submit2'])){
        $r_number = mysqli_real_escape_string($conn, $_POST['r_num']);
        $date_finished = date("Y-m-d"); 
        $person_in_charge = $_SESSION['name'];

        $query = "UPDATE reservations SET status = 'finished' , date_finished = '$date_finished' , 
                    person_in_charge = '$person_in_charge'
                 WHERE r_number = '$r_number';";
                 
        $result = $conn->query($query);

        if(!$result){
            echo "Error<br>";
        }
    }

    if(isset($_POST['logout'])){
        unset($_SESSION['name']);
        unset($_SESSION['usertype']);
        session_destroy();
        header("location:home.php");
    }

?>