<?php session_start();

include "../controllers/add_accounts_function.php"; 

    if(isset($_SESSION['name'])){
    $date = "0000-00-01";
    $date_check = date("Y-m-d"); 

?>

<!DOCTYPE html>
<html>
    <head>
    <title> D.U.E.T. </title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
		<meta name="author" content="GeeksLabs">
		<meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
		<link rel="shortcut icon" href="img/logo.jpg">
	  
		<!-- Bootstrap CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- bootstrap theme -->
		<link href="css/bootstrap-theme.css" rel="stylesheet">
		<!--external css-->
		<!-- font icon -->
		<link href="css/elegant-icons-style.css" rel="stylesheet" />
		<link href="css/font-awesome.min.css" rel="stylesheet" />
		<!-- full calendar css-->
		<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
		<link href="assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
		<!-- easy pie chart-->
		<link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />
		<!-- owl carousel -->
		<link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
		<link href="css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
		<!-- Custom styles -->
		<link rel="stylesheet" href="css/fullcalendar.css">
		<link href="css/widgets.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
		<link href="css/style-responsive.css" rel="stylesheet" />
		<link href="css/xcharts.min.css" rel=" stylesheet">
		<link href="css/jquery-ui-1.10.4.min.css" rel="stylesheet">
      
    </head>

    <body>
		<!-- container section start -->
		<section id="container" class="">


    <header class="header dark-bg">
    
      <!--logo start-->
      <a href="home.php" class="logo"> <img class= "logo" src= "img/logo.jpg" height= "60" width= "150"
          style= "margin-top: -10%; margin-right: -10%"></a>
      <!--logo end-->


<div class="top-nav notification-row">
        <!-- notificatoin dropdown start-->
        <ul class="nav pull-right top-menu">

          <!-- task notificatoin start -->
          <li id="task_notificatoin_bar" class="home">
            <a class="homeBtn" href="admin.php"> Home
                            
                  <ul class="dropdown-menu extended tasks-bar">
                </a>
            </ul>
          </li>

          <!-- alert notification end-->
          <!-- user login dropdown start-->
          <li class="account">
          <a data-toggle="dropdown" class="accountBtn" href="#">
                          <span class="username"><?php echo $_SESSION['name']; ?></span>
                          <b class="caret"></b>
                      </a>
          <ul class="dropdown-menu extended logout">
            <div class="log-arrow-up"></div>
            <li class="eborder-top">
                <a href="view_my_account.php"><i class="fa fa-user"></i> My Profile</a>
              </li>
              <li>
                <a href="add_event.php"><i class="fa fa-user"></i> Add Events</a>
              </li>
              <?php if($_SESSION['usertype']=="admin"){ ?>
              <li>
                <a href="add_updates.php"><i class="fa fa-user"></i> Post New/Updates</a>
              </li>
              <?php } ?>
              <!--<a href="home.php"><i class="fa fa-sign-out"></i> Log Out</a>-->
              <form action="<?php $_PHP_SELF; ?>" method="POST"><i class="fa fa-sign-out"></i>
                  <input type="submit" name="logout" value="Log Out"/>
               </form>
            </li>
            
          </ul>
        </li>
        <!-- user login dropdown end -->
        </ul>
        <!-- notificatoin dropdown end-->
      </div>
    </header>
    <!--header end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-list"></i>ADMIN </h3>
            
          </div>  
        </div>

        <div class="row">
          <div class="col-lg-12">
            <div class="body-content">
                

                <br>
                <div class="container">
                <button type="button" class="view2"><a style="color:black;" class="menu" href="view_accounts.php">List of Admin Accounts</a></button>
                <button type="button" class="view2"><a style="color:black;" class="menu" href="view_user_accounts.php">List of User Accounts</a></button>
                <button type="button" class="view2"><a style="color:black;" class="menu" href="add_accounts.php">Create Account</a></button>
                <button type="button" class="view2"><a style="color:black;" class="menu" href="update_accounts.php">Change Admin Accounts Details</a></button>
                        <br>
                <div class="container" style= "color:white;">
                    <form action="<?php $_PHP_SELF; ?>" method="POST">
                    <fieldset>
                        <legend><p class="head">Create New Admin Account</p></legend>
                            Name<br>
                            <input type="text" name="name" size="20" required/><br>
                            Age&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspGender<br>
                            <input type="number" name="age" min="18" max="65" required/>
                            <select id="gender" name="gender" required>
                                <option id="male" name="male">Male</option>
                                <option id="female" name="female">Female</option>
                                <option id="others" name="others">Others</option>
                            </select><br>
                            Email<br>
                            <input type="email" name="email" size="20" required/><br>
                            Mobile Number<br>
                            <input type="number" name="mobile" required/><br>
                            Username<br>
                            <input type="text" name="username" size="20" required /><br>
                            Password<br>
                            <input type="password" name="password" size="20" minlength="8" required/><br>
                            Confirm Password<br>
                            <input type="password" name="confirm" size="20" minlength="8" required/><br><br>
            
                            <input type="submit" name="submit" value="Create Account" >
                            <a href="view_accounts.php" style="color:black">Cancel</a>
            
                        </fieldset>
                    </form>
                </div>
                        <br>
                
                    <?php 
                      
                        if(isset($password_check)&&!$password_check){
                            echo "<h3>&nbsp&nbsp&nbspConfirm password not match</h3><br>";
                        } 
                        if(isset($password_check)&&$password_check){
                          echo "<h3>&nbsp&nbsp&nbspAccount successfully created.</h3><br>";
                        }
                        if(isset($success_check)&&!$success_check){
                            echo "Error<br>";
                        }
                    ?>
                               
            </div>
          </div>
        </div>


    <!--main content end-->
  </section>
  <!-- container section start -->

	<?php 
		if(isset($_POST['logout'])){
			unset($_SESSION['name']);
			unset($_SESSION['usertype']);
			session_destroy();
		}

  }
    else {
	        header('location:home.php');
        } 
      ?>

<!-- javascripts -->
<script src="js/jquery.js"></script>
  <script src="js/jquery-ui-1.10.4.min.js"></script>
  <script src="js/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
  <!-- bootstrap -->
  <script src="js/bootstrap.min.js"></script>
  <!-- nice scroll -->
  <script src="js/jquery.scrollTo.min.js"></script>
  <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
  <!-- charts scripts -->
  <script src="assets/jquery-knob/js/jquery.knob.js"></script>
  <script src="js/jquery.sparkline.js" type="text/javascript"></script>
  <script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
  <script src="js/owl.carousel.js"></script>
  <!-- jQuery full calendar -->
  <script src="js/fullcalendar.min.js"></script>
    <!-- Full Google Calendar - Calendar -->
    <script src="assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
    <!--script for this page only-->
    <script src="js/calendar-custom.js"></script>
    <script src="js/jquery.rateit.min.js"></script>
    <!-- custom select -->
    <script src="js/jquery.customSelect.min.js"></script>
    <script src="assets/chart-master/Chart.js"></script>

    <!--custome script for all page-->
    <script src="js/scripts.js"></script>
    <!-- custom script for this page-->
    <script src="js/sparkline-chart.js"></script>
    <script src="js/easy-pie-chart.js"></script>
    <script src="js/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="js/jquery-jvectormap-world-mill-en.js"></script>
    <script src="js/xcharts.min.js"></script>
    <script src="js/jquery.autosize.min.js"></script>
    <script src="js/jquery.placeholder.min.js"></script>
    <script src="js/gdp-data.js"></script>
    <script src="js/morris.min.js"></script>
    <script src="js/sparklines.js"></script>
    <script src="js/charts.js"></script>
    <script src="js/jquery.slimscroll.min.js"></script>
    <script>
      //knob
      $(function() {
        $(".knob").knob({
          'draw': function() {
            $(this.i).val(this.cv + '%')
          }
        })
      });

      //carousel
      $(document).ready(function() {
        $("#owl-slider").owlCarousel({
          navigation: true,
          slideSpeed: 300,
          paginationSpeed: 400,
          singleItem: true

        });
      });

      //custom select box

      $(function() {
        $('select.styled').customSelect();
      });

      /* ---------- Map ---------- */
      $(function() {
        $('#map').vectorMap({
          map: 'world_mill_en',
          series: {
            regions: [{
              values: gdpData,
              scale: ['#000', '#000'],
              normalizeFunction: 'polynomial'
            }]
          },
          backgroundColor: '#eef3f7',
          onLabelShow: function(e, el, code) {
            el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
          }
        });
      });
    </script>

    </body>
</html>