<?php session_start();
    include "../models/dbconnect.php";
    include "../controllers/add_updates_function.php";

    if(!isset($_SESSION['name'])){
  
      $query = "SELECT * FROM updates;";
      $result = $conn->query($query);
    
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="img/logo.jpg">

  <title> D.U.E.T. </title>

  <!-- Bootstrap CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="css/elegant-icons-style.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <!-- full calendar css-->
  <link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
  <link href="assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
  <!-- easy pie chart-->
  <link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />
  <!-- owl carousel -->
  <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
  <link href="css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
  <!-- Custom styles -->
  <link rel="stylesheet" href="css/fullcalendar.css">
  <link href="css/widgets.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet" />
  <link href="css/xcharts.min.css" rel=" stylesheet">
  <link href="css/jquery-ui-1.10.4.min.css" rel="stylesheet">
</head>

<body>
  <!-- container section start -->
  <section id="container" class="">


    <header class="header dark-bg">
    
      <!--logo start-->
      <a href="home.php" class="logo"> <img class= "logo" src= "img/logo.jpg" height= "60" width= "150"
          style= "margin-top: -10%; margin-right: -10%"></a>
      <!--logo end-->


      <div class="top-nav notification-row">
        <!-- notificatoin dropdown start-->
        <ul class="nav pull-right top-menu">

          <!-- task notificatoin start -->

          </li>
          <!-- alert notification end-->
          <!-- alert notification start-->
          <li id="alert_notificatoin_bar" class="sign_in">
            <a class="devBtn" href="sign_in.php"> Log In

            <ul class="dropdown-menu extended notification">
             
                  </a>
            
            </ul>
          </li>
          <!-- alert notification end-->
          <!-- alert notification start-->
          <li id="alert_notificatoin_bar" class="sign_up">
            <a class="devBtn" href="sign_up.php"> Sign Up

            <ul class="dropdown-menu extended notification">
             
                  </a>
            
            </ul>
          </li>
        </ul>
        <!-- mail_notificatoin_bar end-->
        <!-- notificatoin dropdown end-->
      </div>
    </header>
    <!--header end-->

   

    <!--main content start-->

      <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header">  </h3>
            <!--<ol class="breadcrumb">-->
                <center>
                <span class="small">D &nbsp &nbsp U &nbsp &nbsp E &nbsp &nbsp T </span>
                <br>
        <span class="big">DAVAO UPCOMING E-SPORTS TOURNAMENT</span>
                </center>

            <!--</ol>-->
          </div>  
        </div>
        <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header">  </h3>
            <!--<ol class="breadcrumb">-->
                <center>
                <br> <br>
                <span class="medium">NEWS & UPDATES</span>
                <br> <br>
                </center>
            <!--</ol>-->
          </div>  
        </div>
       
        <div class="row">
          <div class="col-lg-12">
            <div class="body-content" style="color:black;">

                 <div class="container" style="color:white;">
            <?php if ($result->num_rows>0) { ?>
            <table class="table">
                <thead>
                    <tr>
                        <th>Title of Event</th>
                        <th>Name of Winner</th>
                        <th>Date</th>
                    </tr>
                </thead>
                        <tbody>
                            <?php 
                                while($row=$result->fetch_array()){ 
                            ?>
                                <tr>
                                    <td><?php echo $row['1']; ?></td>
                                    <td><?php echo $row['2']; ?></td>
                                    <td><?php echo $row['3']; ?></td>

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div><br>
                <div class="container">
    <?php }  else { ?>
                    
                    <table class="table" style="color:white;">
                        <thead>
                            <tr>
                            <th>Title of Event</th>
                            <th>Name of Winner</th>
                            <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>------------</td>
                                <td>------------</td>
                                <td>------------</td>
                            </tr>
                        </tbody>
                    </table>
                   
                    
    <?php } ?>
    </div><br>

            </div>
          </div>
        </div>


        

    <!--main content end-->
  </section>
  <!-- container section start -->

  <!-- javascripts -->
  <script src="js/jquery.js"></script>
  <script src="js/jquery-ui-1.10.4.min.js"></script>
  <script src="js/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
  <!-- bootstrap -->
  <script src="js/bootstrap.min.js"></script>
  <!-- nice scroll -->
  <script src="js/jquery.scrollTo.min.js"></script>
  <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
  <!-- charts scripts -->
  <script src="assets/jquery-knob/js/jquery.knob.js"></script>
  <script src="js/jquery.sparkline.js" type="text/javascript"></script>
  <script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
  <script src="js/owl.carousel.js"></script>
  <!-- jQuery full calendar -->
  <script src="js/fullcalendar.min.js"></script>
    <!-- Full Google Calendar - Calendar -->
    <script src="assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
    <!--script for this page only-->
    <script src="js/calendar-custom.js"></script>
    <script src="js/jquery.rateit.min.js"></script>
    <!-- custom select -->
    <script src="js/jquery.customSelect.min.js"></script>
    <script src="assets/chart-master/Chart.js"></script>

    <!--custome script for all page-->
    <script src="js/scripts.js"></script>
    <!-- custom script for this page-->
    <script src="js/sparkline-chart.js"></script>
    <script src="js/easy-pie-chart.js"></script>
    <script src="js/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="js/jquery-jvectormap-world-mill-en.js"></script>
    <script src="js/xcharts.min.js"></script>
    <script src="js/jquery.autosize.min.js"></script>
    <script src="js/jquery.placeholder.min.js"></script>
    <script src="js/gdp-data.js"></script>
    <script src="js/morris.min.js"></script>
    <script src="js/sparklines.js"></script>
    <script src="js/charts.js"></script>
    <script src="js/jquery.slimscroll.min.js"></script>
    <script>
      //knob
      $(function() {
        $(".knob").knob({
          'draw': function() {
            $(this.i).val(this.cv + '%')
          }
        })
      });

      //carousel
      $(document).ready(function() {
        $("#owl-slider").owlCarousel({
          navigation: true,
          slideSpeed: 300,
          paginationSpeed: 400,
          singleItem: true

        });
      });

      //custom select box

      $(function() {
        $('select.styled').customSelect();
      });

      /* ---------- Map ---------- */
      $(function() {
        $('#map').vectorMap({
          map: 'world_mill_en',
          series: {
            regions: [{
              values: gdpData,
              scale: ['#000', '#000'],
              normalizeFunction: 'polynomial'
            }]
          },
          backgroundColor: '#eef3f7',
          onLabelShow: function(e, el, code) {
            el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
          }
        });
      });
    </script>
<?php 
    } else {
        header("location:sign_in.php");
    }
?>
</body>

</html>